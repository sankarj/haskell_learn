sqrt' x = head (dropWhile (not . goodEnough) sqrtGuesses)
         where
           goodEnough guess = (abs (x - guess*guess))/x < 0.00001
           improve guess = (guess + x/guess)/2.0
           sqrtGuesses = 1:(map improve sqrtGuesses)
