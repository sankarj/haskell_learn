units, teens, tens :: [String]
units = ["zero", "one", "two", "three", "four","five","six","seven","eight","nine"]
teens = ["ten", "eleven", "twelve", "thirteen", "fourteen","fifteen","sixteen","seventeen","eighteen","nineteen"]
tens = ["twenty", "thirty", "forty", "fifty", "sixty","seventy","eighty","ninety"]

convert1 :: Int -> String
convert1 n  = units!!n

digit2 :: Int -> (Int, Int)
digit2 n = (div n 10, mod n 10)

convert2 :: Int -> String
convert2 = combine2 . digit2

combine2 :: (Int, Int) -> String
combine2 (t, u)
  | t == 0 = units!!u
  | t == 1 = teens!!u
  | t >= 2 && u == 0 = tens!! (t - 2)
  | t >= 2 && u /= 0 = tens!! (t - 2) ++ "-" ++ units !! u

convert3 :: Int -> String
convert3 n
 | h == 0 = convert2 t
 | n == 0 = convert2 t
 where (h, t) = (n `div` 100, n `mod` 100)
