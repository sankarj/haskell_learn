palindrome  :: (Eq a) => [a] -> Bool
palindrome xs = foldl (\acc (a,b) -> if a == b then acc else False) True input where input = zip xs (reverse xs)

