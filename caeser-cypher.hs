import Data.List
import Data.Maybe
cipher :: [Char] -> Int -> [Char]
cipher [] _ = []
cipher (x:xs) n = (rotate x n) : (cipher xs n)
index :: Char -> Int
index c = fromJust (elemIndex c ['a'..'z'])
newIndex :: Char -> Int -> Int
newIndex c n = ((index c) + n) `mod` 25
rotate :: Char -> Int -> Char
rotate c n = ['a'..'z'] !! newIndex c n
