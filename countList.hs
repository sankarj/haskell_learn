countList :: [a] ->  Int
countList []  = 0
countList (a:[]) = 1
countList (a:xs) = 1 + countList xs
