reverseList :: [a] -> [a]
reverseList [] = []
reverseList (a:xs) = reverseList xs ++ [a]

reverseList' :: [a] -> [a]
reverseList' xs = foldl (\acc x -> x : acc) [] xs

product' :: (Num a) => [a] -> a
product' xs = foldl (\acc x -> acc * x) 1 xs

product'' :: (Num a) => [a] -> a
product'' = foldl (*) 1

product''' :: (Num a) => [a] -> a
product''' = foldl1 (*)

maximum :: (Ord a) => [a] -> a
maximum = foldl1 (max)

filter' :: (a -> Bool) -> [a] -> [a]
filter' p = foldl (\acc x -> if p x then x : acc else acc) []


-- for writing folds, first write fold () initial_value

last' :: [a] -> a
last' = foldl1 (\acc x -> x)
