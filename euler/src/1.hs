mult :: Int -> Int
mult x = foldl (\acc y -> if (y `mod` 3 ==0 || y `mod` 5 == 0) then acc + y else acc + 0) 0 [1..x]
