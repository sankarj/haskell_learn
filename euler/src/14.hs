import Data.List (maximumBy)
import Data.Function (on)

slow_chain :: Integer -> [Integer]
slow_chain 1 = [1]
slow_chain n
  | even n = n:slow_chain (n `div` 2)
  | odd n = n:slow_chain(n* 3 + 1)


memoized_chain :: Int -> [Int]
memoized_chain  = (map chain [1 .. ] !!)
  where
      chain 1 = [1]
      chain n
          | even n = n:chain (n `div` 2)
          | odd n = n:chain(n* 3 + 1)



numLongChains :: Int  ->[Int]
numLongChains x =  maximumBy (compare `on` length) (map memoized_chain [1..x])
